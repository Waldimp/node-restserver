const { Router } = require('express');
const usuarios = require('../controllers/usuarios');

const router = Router();

router.get('/', usuarios.usuariosGet);

router.put('/:id', usuarios.usuariosPut);

router.post('/', usuarios.usuariosPost);

router.delete('/',usuarios.usuariosDelete);



module.exports = router;